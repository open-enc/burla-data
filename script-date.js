window.onload = function () {
    var feriados = [];
    feriados.push(new Date("2017-04-21 00:00:00"));
    feriados.push(new Date("2017-05-01 00:00:00"));
    feriados.push(new Date("2017-05-31 00:00:00"));
    feriados.push(new Date("2017-06-15 00:00:00"));
    feriados.push(new Date("2017-07-09 00:00:00"));
    feriados.push(new Date("2017-09-07 00:00:00"));
    feriados.push(new Date("2017-09-21 00:00:00"));
    feriados.push(new Date("2017-10-12 00:00:00"));
    feriados.push(new Date("2017-11-02 00:00:00"));
    feriados.push(new Date("2017-11-15 00:00:00"));
    feriados.push(new Date("2017-12-19 00:00:00"));
    feriados.push(new Date("2017-12-25 00:00:00"));
    
    //Ontem
    var ontem = new Date();
    ontem.setHours(0, 0, 0);
    ontem.setDate(ontem.getDate() - 1);
    var diaOntemSemana = ontem.getDay();
    
    // se ontem == sabado
    if(diaOntemSemana == 6){
        ontem.setDate(ontem.getDate() - 1);
    }
    // se ontem == domingo
    else if(diaOntemSemana == 0){
        ontem.setDate(ontem.getDate() - 2);
    }

    if(checkFeriado(ontem) == true){
        ontem.setDate(ontem.getDate() - 1);
    }

    var diaOntem = ontem.getDate();
    var mesOntem = ontem.getMonth() + 1;
    var anoOntem = ontem.getFullYear();
    var diaOntemSemana = ontem.getDay();
    var dataOntem = montarData(diaOntem, mesOntem, anoOntem);
    var el = document.getElementsByClassName("data-atualizacao");
    for (var i = 0; i < el.length; i++) {
        document.getElementsByClassName("data-atualizacao")[i].innerHTML = dataOntem;
    }
    
    function montarData(dia, mes, ano){
        mes = mes < 10 ? "0" + mes : mes;
        var data = dia + "/" + mes + "/" + ano;
        return data;
    }

    function checkFeriado(data) {
        var i;
        for(i = 0; i < feriados.length; i++){
            var dataFeriado = montarData(feriados[i].getDate(), feriados[i].getMonth(), feriados[i].getFullYear());
            var dataCheck = montarData(data.getDate(), data.getMonth(), data.getFullYear());
            if(dataFeriado == dataCheck){
                return true;
            }
        }
        return false;
    }
}